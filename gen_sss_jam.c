/**
 *
 * \section COPYRIGHT
 *
 * Copyright 2013-2014 The libLTE Developers. See the
 * COPYRIGHT file at the top-level directory of this distribution.
 *
 * \section LICENSE
 *
 * This file is part of the libLTE library.
 *
 * libLTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * libLTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * A copy of the GNU Lesser General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 *
 */


#include <strings.h>

#include "lte/sync/sss.h"

/**
* This function creates AWGN in place of the secondary synchronization sequences
*/

void sss_generate(cf_t*signal0, cf_t *signal5, double SJR_per_bit) {

	int i;

	for (i = 0; i < N_SSS; i++) {
		/** Even Resource Elements: Sub-frame 0*/
		__real__ signal0[2 * i] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
		__imag__ signal0[2 * i] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();

		/** Odd Resource Elements: Sub-frame 0*/
		__real__ signal0[2 * i + 1] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
		__imag__ signal0[2 * i + 1] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
	}
	for (i = 0; i < N_SSS; i++) {

		/** Even Resource Elements: Sub-frame 5*/
		__real__ signal5[2 * i] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
		__imag__ signal5[2 * i] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();

		/** Odd Resource Elements: Sub-frame 5*/
		__real__ signal5[2 * i + 1] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
		__imag__ signal5[2 * i + 1] = sqrt(1/2/pow(10,(SJR_per_bit/10))/2) * rand_gauss();
	}
}

